TARGET = arm-none-eabi
CC = $(TARGET)-gcc
LD = $(TARGET)-ld
OBJDUMP = $(TARGET)-objdump
OBJCOPY = $(TARGET)-objcopy
LDSCRIPT = tm4c.ld
STARTUP = startup
FLASH = lm4flash -E

CFLAGS ?= -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -mthumb -Wall -Wextra -Os -fdata-sections -ffunction-sections
LDFLAGS ?= --gc-sections --entry ResetISR

.PHONY: clean flash

all: main.bin

main.bin: main.elf
	$(OBJCOPY) $< $@ -O binary

main.elf: main.o $(STARTUP).o
	$(LD) -T $(LDSCRIPT) -o $@ $^ driverlib.lib
	$(OBJDUMP) -D main.elf > main.script

flash: main.bin
	$(FLASH) $<

%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $<

clean:
	@rm -f *.o
	@rm -f *.elf
	@rm -f *.bin
	@rm -f *.script
